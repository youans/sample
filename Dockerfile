from openjdk:11
add target/sample.jar sample.jar
expose 8080
entrypoint ["java","-jar","sample.jar"]