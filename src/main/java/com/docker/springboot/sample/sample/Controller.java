package com.docker.springboot.sample.sample;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by: Youans Ezzat
 * For: Dotsub LLC.
 * Date: 2019-04-18.
 */
@RestController
public class Controller {
    @GetMapping
    public String sayHello(){
        return "Hello from docker!";
    }
}
